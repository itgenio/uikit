/// <reference types="react" />
import { KeyboardDatePickerProps } from '@material-ui/pickers/DatePicker/DatePicker';
export declare const DatePicker: (props: KeyboardDatePickerProps) => JSX.Element;
