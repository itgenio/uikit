import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Fab from '@material-ui/core/Fab';
import ArrowUpward from '@material-ui/icons/ArrowUpward';
const FabStyled = withStyles({
    root: {
        position: 'fixed',
        right: 56,
        bottom: 75,
        width: 64,
        height: 64
    }
})(Fab);
export const FabComponent = ({ onClick, visible }) => {
    if (!visible)
        return null;
    return (React.createElement(FabStyled, { color: "primary", disableRipple: true, children: React.createElement(ArrowUpward, null), onClick: onClick }));
};
//# sourceMappingURL=FabComponent.js.map