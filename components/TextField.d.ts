/// <reference types="react" />
import { TextFieldProps } from '@material-ui/core/TextField';
export declare const TextFieldComponent: (props: TextFieldProps) => JSX.Element;
