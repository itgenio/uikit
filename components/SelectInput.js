import React from 'react';
import { InputLabel, MenuItem, FormControl, Select, ListSubheader, makeStyles } from '@material-ui/core';
const selectStyles = makeStyles(() => ({
    formControl: {
        minWidth: 250
    },
    label: {
        backgroundColor: 'white',
        paddingRight: '7px'
    },
    group: {
        height: '3rem',
        lineHeight: '3rem',
        fontSize: 14,
        color: '#aaa'
    },
    hr: {
        margin: 0,
        marginTop: '.5rem',
        borderColor: '#ccc',
        borderWidth: '.5px'
    }
}));
export const SelectInput = ({ label, specialCaseText, selectedItem, selectOnChange, listOfItems }) => {
    const list = Object.keys(listOfItems);
    const style = selectStyles();
    return (React.createElement(FormControl, { variant: "standard", className: style.formControl },
        React.createElement(InputLabel, { id: "select-outlined-label", className: style.label }, label),
        React.createElement(Select, { labelId: "select-outlined-label", value: selectedItem || '', onChange: selectOnChange },
            list.map((value, index) => [
                value[0] === '_' ? null : (React.createElement(ListSubheader, { className: style.group }, value)),
                listOfItems[value].map(({ locale }) => (React.createElement(MenuItem, { value: locale, key: locale }, locale))),
                index === list.length - 1 ? null : React.createElement("hr", { className: style.hr })
            ]),
            selectedItem === 'special-case' ? (React.createElement(MenuItem, { value: 'special-case', key: 'special-case' }, specialCaseText)) : null)));
};
//# sourceMappingURL=SelectInput.js.map