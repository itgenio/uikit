import { DividerTypeMap } from '@material-ui/core/Divider';
import { OverridableComponent } from '@material-ui/core/OverridableComponent';
export declare const DividerStyled: OverridableComponent<DividerTypeMap>;
