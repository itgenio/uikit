/// <reference types="react" />
export declare const AlertBox: ({ type, messages }: {
    type: "warning" | "info" | "attention" | "success";
    messages: {
        text: string;
        className?: string | undefined;
        link?: string | undefined;
    }[];
}) => JSX.Element;
