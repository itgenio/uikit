import React from 'react';
import Container from '@material-ui/core/Container';
import { makeStyles } from '@material-ui/core';
const styles = makeStyles(() => ({
    root: {
        margin: 'auto',
        position: 'absolute',
        top: '10%'
    }
}));
export const ContainerStyled = (props) => (React.createElement(Container, Object.assign({ classes: styles() }, props)));
//# sourceMappingURL=Container.js.map