/// <reference types="react" />
export declare const Loader: ({ text, size, color }: {
    text?: string | undefined;
    size?: string | undefined;
    color?: "primary" | "secondary" | undefined;
}) => JSX.Element;
