/// <reference types="react" />
import { ContainerProps } from '@material-ui/core';
export declare const ContainerStyled: (props: ContainerProps) => JSX.Element;
