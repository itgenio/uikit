import React from 'react';
import MUISwitch from '@material-ui/core/Switch';
import { withStyles } from '@material-ui/core/styles';
const SwitchStyled = withStyles((theme) => ({
    switchBase: {
        '&$checked': {
            color: theme.palette.primary.main
        },
        '&$checked:hover': {
            backgroundColor: 'rgba(0, 0, 0, 0.08)'
        },
        '&$checked + $track': {
            backgroundColor: theme.palette.primary.main
        }
    },
    checked: {},
    track: {}
}))(MUISwitch);
export const Switch = ({ checked, disabled, onChange }) => {
    return (React.createElement(SwitchStyled, { color: "primary", checked: checked, disabled: disabled, onChange: onChange }));
};
//# sourceMappingURL=Switch.js.map