import React from 'react';
import { TooltipProps } from '@material-ui/core/Tooltip';
declare type Props = Pick<TooltipProps, 'title' | 'placement'> & {
    maxWidth?: number;
};
export declare const TooltipHelp: React.FunctionComponent<Props>;
export {};
