import React from 'react';
export declare const SelectInput: ({ label, specialCaseText, selectedItem, selectOnChange, listOfItems }: {
    label: string;
    specialCaseText: string;
    selectedItem?: string | undefined;
    listOfItems: Record<string, {
        locale: string;
    }[]>;
    selectOnChange: (event: React.ChangeEvent<{
        name?: string | undefined;
        value: unknown;
    }>, child: React.ReactNode) => void;
}) => JSX.Element;
