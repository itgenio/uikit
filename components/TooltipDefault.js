import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Tooltip from '@material-ui/core/Tooltip';
const useStyles = makeStyles(theme => ({
    tooltip: {
        backgroundColor: '#fff',
        color: '#3b3b3b',
        fontSize: 12,
        fontWeight: 500,
        margin: theme.spacing(1),
        border: '1px solid #cfd2d3',
        boxShadow: '0px 2px 20px rgba(0,0,0,0.2)',
        maxWidth: (props) => props.maxWidth || 230
    }
}));
export const TooltipDefault = ({ title, placement, maxWidth, children }) => {
    // TODO: fix types
    //@ts-ignore
    return (React.createElement(Tooltip, { disableFocusListener: true, disableTouchListener: true, placement: placement || 'right', title: title, classes: useStyles({ maxWidth }) }, children));
};
export { Tooltip };
//# sourceMappingURL=TooltipDefault.js.map