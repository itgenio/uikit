import React from 'react';
export declare const IconButtonUploadFile: ({ isReady, isLoading, buttonId, inputId, onClick, onChange }: {
    isReady: boolean;
    isLoading: boolean;
    buttonId: string;
    inputId: string;
    onClick: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
    onChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
}) => JSX.Element;
