/// <reference types="react" />
export declare const FabComponent: ({ onClick, visible }: {
    onClick: () => void;
    visible: boolean;
}) => JSX.Element | null;
