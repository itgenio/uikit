import React from 'react';
import { MuiPickersUtilsProvider, KeyboardDatePicker } from '@material-ui/pickers';
import MomentUtils from '@date-io/moment';
import { CalendarOutlinedIcon } from '../icons';
export const DatePicker = (props) => {
    return (React.createElement(MuiPickersUtilsProvider, { utils: MomentUtils },
        React.createElement(KeyboardDatePicker, Object.assign({ autoOk: true, disableToolbar: true, disableFuture: true, inputVariant: "standard", variant: "inline", keyboardIcon: React.createElement(CalendarOutlinedIcon, null), format: "DD.MM.YYYY" }, props))));
};
//# sourceMappingURL=DatePicker.js.map