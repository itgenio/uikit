import { LinkTypeMap } from '@material-ui/core/Link';
import { OverridableComponent } from '@material-ui/core/OverridableComponent';
export declare const BrandLink: OverridableComponent<LinkTypeMap>;
