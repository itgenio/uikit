import React from 'react';
import SendIcon from '@material-ui/icons/Send';
import IconButton from '@material-ui/core/IconButton';
export const IconButtonSend = ({ onClick, disabled, forwardRef, fontSize }) => (React.createElement(IconButton, { onClick: onClick, disabled: disabled, ref: forwardRef },
    React.createElement(SendIcon, { fontSize: fontSize })));
//# sourceMappingURL=IconButtonSend.js.map