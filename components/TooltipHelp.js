import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Tooltip from '@material-ui/core/Tooltip';
import HelpOutlineOutlined from '@material-ui/icons/HelpOutlineOutlined';
const useStyles = makeStyles(theme => ({
    tooltip: {
        backgroundColor: '#fff',
        color: '#3b3b3b',
        fontSize: 12,
        fontWeight: 500,
        margin: theme.spacing(1),
        border: '1px solid #cfd2d3',
        boxShadow: '0px 2px 20px rgba(0,0,0,0.2)',
        maxWidth: (props) => props.maxWidth || 230
    }
}));
export const TooltipHelp = ({ title, maxWidth, placement }) => (React.createElement(Tooltip, { disableFocusListener: true, disableTouchListener: true, placement: placement || 'right', title: title, classes: useStyles({ maxWidth }) },
    React.createElement(HelpOutlineOutlined, { color: "primary", style: { width: '20px', height: '20px' } })));
//# sourceMappingURL=TooltipHelp.js.map