/// <reference types="react" />
import { SwitchProps } from '@material-ui/core/Switch';
export declare const Switch: ({ checked, disabled, onChange }: Pick<SwitchProps, "disabled" | "onChange" | "checked">) => JSX.Element;
