import React from 'react';
import IconButton from '@material-ui/core/IconButton';
import AttachFileIcon from '@material-ui/icons/AttachFile';
import DeleteIcon from '@material-ui/icons/Delete';
import { Loader } from './Loader';
export const IconButtonUploadFile = ({ isReady, isLoading, buttonId, inputId, onClick, onChange }) => (React.createElement(IconButton, { tabIndex: 0, id: buttonId, onClick: onClick },
    isReady ? (React.createElement(DeleteIcon, { fontSize: "large" })) : isLoading ? (React.createElement(Loader, null)) : (React.createElement(AttachFileIcon, { fontSize: "large" })),
    React.createElement("input", { tabIndex: -1, type: "file", id: inputId, style: { display: 'none' }, onChange: onChange })));
//# sourceMappingURL=IconButtonUploadFile.js.map