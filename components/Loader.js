import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
export const Loader = ({ text = '', size = '1.5rem', color = 'primary' }) => (React.createElement("span", { style: { display: 'inline-flex', alignItems: 'center' } },
    React.createElement(CircularProgress, { size: size, color: color }),
    !text ? null : React.createElement("span", { style: { marginLeft: '10px' } }, text)));
//# sourceMappingURL=Loader.js.map