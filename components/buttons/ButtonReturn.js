import React from 'react';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardBackspace';
import IconButton from '@material-ui/core/IconButton';
export const ButtonReturn = ({ onClick }) => (React.createElement(IconButton, { color: "inherit", onClick: onClick || (() => window.history.back()), children: React.createElement(KeyboardArrowLeft, null) }));
//# sourceMappingURL=ButtonReturn.js.map