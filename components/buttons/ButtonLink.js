import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
const buttonLinkStyles = makeStyles((theme) => ({
    root: {
        border: theme.buttons.link.border,
        '&:hover': {
            backgroundColor: theme.buttons.link.backgroundColorHover
        },
        '&:active': {
            backgroundColor: theme.buttons.link.backgroundColorPressed
        }
    },
    label: {
        fontWeight: 'normal'
    },
    disabled: {
        backgroundColor: theme.buttons.link.backgroundColorDisabled,
        border: theme.buttons.link.borderDisabled
    }
}));
export const ButtonLink = (props) => {
    const classes = buttonLinkStyles(props);
    return (React.createElement(Button, Object.assign({ variant: "outlined", classes: {
            root: classes.root,
            label: classes.label,
            disabled: classes.disabled
        } }, props), props.children));
};
//# sourceMappingURL=ButtonLink.js.map