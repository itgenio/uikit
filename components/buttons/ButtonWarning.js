import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
export const ButtonWarning = withStyles((theme) => ({
    root: {
        backgroundColor: theme.buttons.warning.backgroundColor,
        boxShadow: theme.buttons.warning.boxShadow,
        '&:hover': {
            backgroundColor: theme.buttons.warning.backgroundColorHover,
            boxShadow: theme.buttons.warning.boxShadowHover
        },
        '&:active': {
            backgroundColor: theme.buttons.warning.backgroundColorPressed,
            boxShadow: 'none'
        }
    },
    label: {
        color: theme.buttons.warning.color
    },
    disabled: {
        color: theme.buttons.warning.colorDisabled,
        backgroundColor: theme.buttons.warning.backgroundColorDisabled
    }
}))(Button);
//# sourceMappingURL=ButtonWarning.js.map