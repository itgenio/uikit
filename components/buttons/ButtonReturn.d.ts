/// <reference types="react" />
export declare const ButtonReturn: ({ onClick }: {
    onClick?: (() => void) | undefined;
}) => JSX.Element;
