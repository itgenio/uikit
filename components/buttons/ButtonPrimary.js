import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
export const ButtonPrimary = withStyles((theme) => ({
    root: {
        backgroundColor: theme.buttons.primary.backgroundColor,
        boxShadow: theme.buttons.primary.boxShadow,
        '&:hover': {
            backgroundColor: theme.buttons.primary.backgroundColorHover,
            boxShadow: theme.buttons.primary.boxShadowHover
        },
        '&:active': {
            backgroundColor: theme.buttons.primary.backgroundColorPressed,
            boxShadow: 'none'
        }
    },
    label: {
        color: theme.buttons.primary.color
    },
    disabled: {
        color: theme.buttons.primary.colorDisabled,
        backgroundColor: theme.buttons.primary.backgroundColorDisabled
    }
}))(Button);
//# sourceMappingURL=ButtonPrimary.js.map