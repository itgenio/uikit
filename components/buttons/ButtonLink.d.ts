/// <reference types="react" />
export declare const ButtonLink: (props: import("@material-ui/core/OverridableComponent").OverrideProps<import("@material-ui/core/Button").ButtonTypeMap<{}, "button">, "button">) => JSX.Element;
