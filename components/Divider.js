import React from 'react';
import Divider from '@material-ui/core/Divider';
import { makeStyles } from '@material-ui/core/styles';
const styles = makeStyles(() => ({
    root: {
        backgroundColor: 'unset',
        borderTop: '2px solid',
        opacity: '0.1'
    }
}));
export const DividerStyled = (props) => React.createElement(Divider, Object.assign({ classes: styles() }, props));
//# sourceMappingURL=Divider.js.map