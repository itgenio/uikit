import React from 'react';
import Tooltip, { TooltipProps } from '@material-ui/core/Tooltip';
declare type Props = Pick<TooltipProps, 'title' | 'placement'> & {
    maxWidth?: number;
};
export declare const TooltipDefault: React.ComponentType<Props>;
export { Tooltip };
