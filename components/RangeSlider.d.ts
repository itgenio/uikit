import React from 'react';
export declare const RangeSlider: ({ value, min, max, step, steps, onChange, onChangeCommitted }: {
    value: number | number[];
    min?: number | undefined;
    max?: number | undefined;
    step?: number | undefined;
    steps: {
        value: number;
        label?: string | undefined;
    }[];
    onChange?: ((event: React.ChangeEvent<{}>, value: number | number[]) => void) | undefined;
    onChangeCommitted?: ((event: React.ChangeEvent<{}>, value: number | number[]) => void) | undefined;
}) => JSX.Element;
