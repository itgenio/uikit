import React from 'react';
import Link from '@material-ui/core/Link';
import { makeStyles } from '@material-ui/core';
import { darken } from '@material-ui/core/styles/colorManipulator';
const brandColor = '#337ab7';
const useStyles = makeStyles({
    root: {
        color: brandColor,
        cursor: 'pointer',
        textDecoration: 'none',
        '&:hover': {
            color: darken(brandColor, 0.1),
            textDecoration: 'none'
        }
    }
});
export const BrandLink = (props) => (
// TODO: prop underline does not work on portal with hover
React.createElement(Link, Object.assign({ underline: "none", classes: useStyles() }, props)));
//# sourceMappingURL=Link.js.map