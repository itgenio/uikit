import React from 'react';
import { SvgIconProps } from '@material-ui/core';
export declare const IconButtonSend: ({ onClick, disabled, forwardRef, fontSize }: Pick<import("@material-ui/core/OverridableComponent").OverrideProps<import("@material-ui/core").IconButtonTypeMap<{}, "button">, "button">, "disabled" | "onClick"> & Pick<SvgIconProps, "fontSize"> & {
    forwardRef?: React.MutableRefObject<HTMLButtonElement | null> | undefined;
}) => JSX.Element;
