import React from 'react';
import { Loader } from '../components/Loader';
import { themeDecorator } from './themeDecorator';

export default {
  title: 'Loader',
  component: Loader,
  decorators: [themeDecorator]
};

export const Default = () => <Loader />;

export const WithText = () => <Loader text={"I'm loader"} />;

export const Secondry = () => <Loader text={"I'm loader"} color="secondary" />;
