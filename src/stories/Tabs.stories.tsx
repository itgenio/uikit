import React, { useState } from 'react';
import { themeDecorator } from './themeDecorator';
import { TabsPrimary, TabPrimary } from '../components';

export default {
  title: 'Tabs',
  decorators: [themeDecorator]
};

const pages = ['1 page', '2 page', '3 page', '4 pafe', '5 page', '6 page'];

export const TabsStandart = () => {
  const [current, setCurrent] = useState(pages[0]);

  return (
    <TabsPrimary
      variant="standard"
      value={current}
      onChange={(_e, newTab) => setCurrent(newTab)}
      children={pages.map((item, idx) => (
        <TabPrimary key={idx} value={item} label={item} />
      ))}
    />
  );
};

export const TabsFullWidth = () => {
  const [current, setCurrent] = useState(pages[0]);

  return (
    <TabsPrimary
      variant="fullWidth"
      value={current}
      onChange={(_e, newTab) => setCurrent(newTab)}
      children={pages.map((item, idx) => (
        <TabPrimary key={idx} value={item} label={item} />
      ))}
    />
  );
};

export const TabslScrollable = () => {
  const [current, setCurrent] = useState(pages[0]);

  return (
    <div style={{ width: '300px' }}>
      <TabsPrimary
        variant="scrollable"
        value={current}
        onChange={(_e, newTab) => setCurrent(newTab)}
        children={pages.map((item, idx) => (
          <TabPrimary key={idx} value={item} label={item} />
        ))}
      />
    </div>
  );
};
