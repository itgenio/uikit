import React, { useState } from 'react';

import { themeDecorator } from './themeDecorator';
import { DatePicker } from '../components/DatePicker';

export default {
  title: 'DatePicker',
  decorators: [themeDecorator]
};

export const CustormDatePicker = () => {
  const [date, setDate] = useState(new Date());

  return (
    <DatePicker
      label={'date picker'}
      onChange={date => date && setDate(date.toDate())}
      value={date}
      disableFuture={false}
      minDateMessage={'min date message'}
    />
  );
};
