import React from 'react';
import { ThemeProvider } from '@material-ui/core';
import { DefaultTheme } from '../themes/default';

export const themeDecorator = (storyFn: () => JSX.Element) => (
  <ThemeProvider theme={DefaultTheme}>{storyFn()}</ThemeProvider>
);
