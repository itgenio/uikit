import React from 'react';
import { Switch } from '../components/Switch';
import { themeDecorator } from './themeDecorator';

export default {
  title: 'Switch',
  component: Switch,
  decorators: [themeDecorator]
};

export const Default = () => <Switch />;

export const Checked = () => <Switch checked />;

export const Disabled = () => <Switch disabled />;

export const CheckedDisabled = () => <Switch checked disabled />;
