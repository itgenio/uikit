import React from 'react';
import { themeDecorator } from './themeDecorator';
import { TooltipHelp, TooltipDefault } from '../components';

export default {
  title: 'ToolTipComponent',
  decorators: [themeDecorator]
};

export const ToolTipRight = () => (
  <TooltipHelp title="Lorem nostrud occaecat consectetur eiusmod tempor labore sint exercitation excepteur et nisi fugiat dolore. Nulla duis exercitation adipisicing sint labore irure. Do duis eu minim deserunt proident. Id veniam amet aliqua anim aliqua esse cillum eiusmod nulla irure id incididunt. Nulla do culpa esse cillum nostrud. Ut laborum officia id minim reprehenderit eu consectetur nulla tempor nulla anim occaecat. Elit eiusmod sit velit ad cillum consequat cillum minim veniam minim." />
);

export const ToolTipDown = () => (
  <TooltipHelp
    title="Cillum Lorem exercitation ullamco Lorem aute qui amet laborum commodo excepteur. Reprehenderit excepteur dolor ullamco quis ea. Ex commodo consectetur exercitation amet nisi est. Et non ea in dolor velit officia labore sit anim Lorem cupidatat excepteur eiusmod qui. Commodo laborum proident labore non quis ea. Ut reprehenderit veniam consectetur dolor culpa duis aliquip occaecat nostrud laboris elit."
    placement="bottom"
  />
);

export const ToolTipThin = () => (
  <TooltipHelp
    title="Cillum Lorem exercitation ullamco Lorem aute qui amet laborum commodo excepteur. Reprehenderit excepteur dolor ullamco quis ea. Ex commodo consectetur exercitation amet nisi est. Et non ea in dolor velit officia labore sit anim Lorem cupidatat excepteur eiusmod qui. Commodo laborum proident labore non quis ea. Ut reprehenderit veniam consectetur dolor culpa duis aliquip occaecat nostrud laboris elit."
    placement="bottom"
    maxWidth={100}
  />
);

export const ToolTipWide = () => (
  <TooltipHelp
    title="Cillum Lorem exercitation ullamco Lorem aute qui amet laborum commodo excepteur. Reprehenderit excepteur dolor ullamco quis ea. Ex commodo consectetur exercitation amet nisi est. Et non ea in dolor velit officia labore sit anim Lorem cupidatat excepteur eiusmod qui. Commodo laborum proident labore non quis ea. Ut reprehenderit veniam consectetur dolor culpa duis aliquip occaecat nostrud laboris elit.Cillum Lorem exercitation ullamco Lorem aute qui amet laborum commodo excepteur. Reprehenderit excepteur dolor ullamco quis ea. Ex commodo consectetur exercitation amet nisi est. Et non ea in dolor velit officia labore sit anim Lorem cupidatat excepteur eiusmod qui. Commodo laborum proident labore non quis ea. Ut reprehenderit veniam consectetur dolor culpa duis aliquip occaecat nostrud laboris elit."
    placement="bottom"
    maxWidth={500}
  />
);

export const ParagraphWithToolTip = () => (
  <p>
    Deserunt anim cupidatat sit quis esse cillum veniam consequat non anim aute
    ad.
    <TooltipHelp title="Nostrud voluptate voluptate pariatur veniam ea mollit sint." />
  </p>
);

export const WithChildren = () => (
  <TooltipDefault title="Quis proident irure culpa incididunt magna dolore.">
    <span
      style={{
        border: '.5px solid rgba(0, 0, 0, 0.2)',
        backgroundColor: 'rgba(200, 0, 0, 0.1)',
        padding: '3px 6px'
      }}
    >
      Lorem
    </span>
  </TooltipDefault>
);
