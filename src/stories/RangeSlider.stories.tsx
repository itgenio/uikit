import React, { useState } from 'react';
import { themeDecorator } from './themeDecorator';
import { RangeSlider } from '../components/RangeSlider';
import { action } from '@storybook/addon-actions';
import { isArray } from 'util';

export default {
  title: 'RangeSlider',
  decorators: [themeDecorator]
};

export const RangeSliderComponent = () => {
  const [values, setValues] = useState([1, 2]);

  const handlerChange = (
    event: React.ChangeEvent<{}>,
    value: number | number[]
  ) => {
    if (!isArray(value)) {
      value = [value];
    }

    setValues(value);
  };

  return (
    <div style={{ width: '300px' }}>
      <RangeSlider
        value={values}
        min={0}
        max={10}
        step={1}
        onChange={handlerChange}
        steps={[
          { value: 0, label: 'begin' },
          { value: 5, label: 'middle' },
          { value: 10, label: 'end' }
        ]}
      />
    </div>
  );
};
