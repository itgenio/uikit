import React from 'react';
import { themeDecorator } from './themeDecorator';
import { AlertBox } from '../components';

export default {
  title: 'Alert warning',
  decorators: [themeDecorator]
};

export const Warning = () => (
  <AlertBox type="warning" messages={[{ text: 'Simple warning alert' }]} />
);

export const Info = () => (
  <AlertBox type="info" messages={[{ text: 'Simple info alert' }]} />
);

export const Attention = () => (
  <AlertBox
    type="attention"
    messages={[
      { text: 'Simple attention alert' },
      { text: 'Another attention alert' }
    ]}
  />
);

export const Success = () => (
  <AlertBox type="success" messages={[{ text: 'Simple success alert' }]} />
);

export const SuccessWithLink = () => (
  <AlertBox
    type="success"
    messages={[
      { link: 'https://google.com', text: 'It is link to google.com' }
    ]}
  />
);
