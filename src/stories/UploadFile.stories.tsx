import React from 'react';
import { action } from '@storybook/addon-actions';
import { themeDecorator } from './themeDecorator';
import { IconButtonUploadFile } from '../components';

export default {
  title: 'UploadFileComponent',
  decorators: [themeDecorator]
};

export const UploadFileButton = () => (
  <IconButtonUploadFile
    buttonId="button-id"
    inputId="input-id"
    isLoading={false}
    isReady={false}
    onChange={action('onChange')}
    onClick={action('onClick')}
  />
);

export const UploadFileReady = () => (
  <IconButtonUploadFile
    buttonId="button-id"
    inputId="input-id"
    isLoading={false}
    isReady={true}
    onChange={action('onChange')}
    onClick={action('onClick')}
  />
);

export const UploadFileLoadig = () => (
  <IconButtonUploadFile
    buttonId="button-id"
    inputId="input-id"
    isLoading={true}
    isReady={false}
    onChange={action('onChange')}
    onClick={action('onClick')}
  />
);
