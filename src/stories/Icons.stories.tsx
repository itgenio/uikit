import React from 'react';
import * as Icons from '../icons';
import { themeDecorator } from './themeDecorator';

export default {
  title: 'Icons',
  decorators: [themeDecorator]
};

export const AllIcons = () => (
  <div
    style={{
      display: 'flex',
      justifyContent: 'space-between',
      width: '700px'
    }}
  >
    <Icons.ArrowLeftIosIcon />
    <Icons.ArrowRightIosIcon />
    <Icons.AssignmentOutlinedIcon />
    <Icons.BookOpenedOutlinedIcon />
    <Icons.BusinessBagOutlinedIcon />
    <Icons.CalendarOutlinedIcon />
    <Icons.CheckFilledIcon />
    <Icons.CloseRoundedIcon />
    <Icons.DiagramOutlinedIcon />
    <Icons.ExpandLessIcon />
    <Icons.ExpandMoreIcon />
    <Icons.FillingCheckboxOutlinedIcon />
    <Icons.FolderOutlinedIcon />
    <Icons.GraduationCapOutlinedIcon />
    <Icons.HomeOutlinedIcon />
    <Icons.MenuOutlinedIcon />
    <Icons.PersonSearchOutlinedIcon />
    <Icons.PlayCircleFilledRoundedIcon />
    <Icons.ProfileOutlinedIcon />
    <Icons.SearchOutlinedIcon />
    <Icons.ShoppingCarOutlinedIcon />
  </div>
);

export const ArrowLeftIosIcon = () => <Icons.ArrowLeftIosIcon />;
export const ArrowRightIosIcon = () => <Icons.ArrowRightIosIcon />;
export const AssignmentOutlined = () => <Icons.AssignmentOutlinedIcon />;
export const BookOpenedOutlined = () => <Icons.BookOpenedOutlinedIcon />;
export const BusinesBagoutlined = () => <Icons.BusinessBagOutlinedIcon />;
export const CalendarOutlined = () => <Icons.CalendarOutlinedIcon />;
export const ChatOutlined = () => <Icons.ChatOutlinedIcon />;
export const CloseRounded = () => <Icons.CloseRoundedIcon />;
export const CheckFilledIcon = () => <Icons.CheckFilledIcon />;
export const DiagramOutlined = () => <Icons.DiagramOutlinedIcon />;
export const ExpandLess = () => <Icons.ExpandLessIcon />;
export const ExpandMore = () => <Icons.ExpandMoreIcon />;
export const FillingCheckboxOutlined = () => (
  <Icons.FillingCheckboxOutlinedIcon />
);
export const FolderOutlined = () => <Icons.FolderOutlinedIcon />;
export const GraduationCapOutlined = () => <Icons.GraduationCapOutlinedIcon />;
export const HomeOutlined = () => <Icons.HomeOutlinedIcon />;
export const MenuOutlined = () => <Icons.MenuOutlinedIcon />;
export const PersonSearchOutlined = () => <Icons.PersonSearchOutlinedIcon />;
export const PlayCircleFilledRoundedIcon = () => (
  <Icons.PlayCircleFilledRoundedIcon />
);
export const ProfileOutlinedIcon = () => <Icons.ProfileOutlinedIcon />;
export const SearchOutlinedIcon = () => <Icons.SearchOutlinedIcon />;
export const ShoppingCarOutlinedIcon = () => <Icons.ShoppingCarOutlinedIcon />;
