import React, { useState } from 'react';
import { themeDecorator } from './themeDecorator';
import { TextField } from '../components';

export default {
  title: 'TextField',
  decorators: [themeDecorator]
};

export const SimpleTextField = () => {
  const [text, setText] = useState(
    'Nulla dolor tempor adipisicing elit laboris et adipisicing officia dolore incididunt culpa ea nulla eiusmod.'
  );

  return (
    <TextField
      value={text}
      onChange={event => setText(event.target.value)}
      placeholder="Type here anything"
    />
  );
};

export const FullWidthTextField = () => {
  const [text, setText] = useState(
    'Nulla dolor tempor adipisicing elit laboris et adipisicing officia dolore incididunt culpa ea nulla eiusmod.'
  );

  return (
    <TextField
      value={text}
      onChange={event => setText(event.target.value)}
      placeholder="Type here anything"
      fullWidth
    />
  );
};

export const DisabledTextField = () => {
  const [text, setText] = useState(
    'Nulla dolor tempor adipisicing elit laboris et adipisicing officia dolore incididunt culpa ea nulla eiusmod.'
  );

  return (
    <TextField
      value={text}
      onChange={event => setText(event.target.value)}
      placeholder="Type here anything"
      disabled
    />
  );
};

export const EmptyTextField = () => {
  const [text, setText] = useState('');

  return (
    <TextField
      value={text}
      onChange={event => setText(event.target.value)}
      placeholder="Type here anything"
    />
  );
};

export const MultilineTextField = () => {
  const [text, setText] = useState(
    'In qui est voluptate officia anim magna voluptate reprehenderit ad consectetur ad irure enim magna. Id nulla dolore culpa dolor in. Velit ad do minim veniam esse qui cupidatat pariatur aliquip commodo. Exercitation irure officia mollit qui amet exercitation veniam et. Irure esse enim aute ad aute sint enim anim enim ea. Dolor voluptate Lorem sint consequat cillum enim ea qui elit aliqua ea ad. Fugiat in do sint reprehenderit exercitation dolor proident esse ea sit id.'
  );

  return (
    <div style={{ width: '500px' }}>
      <TextField
        value={text}
        onChange={event => setText(event.target.value)}
        placeholder="Type here anything"
        multiline
        fullWidth
      />
    </div>
  );
};
