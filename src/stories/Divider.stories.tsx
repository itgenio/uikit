import React from 'react';
import { themeDecorator } from './themeDecorator';
import { DividerStyled } from '../components';

export default {
  title: 'DividerStyled',
  decorators: [themeDecorator]
};

export const SimpleStyledDivider = () => (
  <ul
    style={{
      width: '200px',
      listStyle: 'none',
      border: '1px solid #ccc',
      padding: '10px',
      margin: '0'
    }}
  >
    <li>First item</li>
    <li>Second item</li>
    <DividerStyled />
    <li>First item</li>
    <li>Second item</li>
  </ul>
);
