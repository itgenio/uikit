import React, { useState } from 'react';

import { themeDecorator } from './themeDecorator';
import { SelectInput } from '../components/SelectInput';

export default {
  title: 'SelectInput',
  decorators: [themeDecorator]
};

const inputLabel = 'Choose something';

export const CustomSelectWithSeparetedList = () => {
  const [selected, setSelected] = useState('');

  return (
    <SelectInput
      label={inputLabel}
      specialCaseText={'Special case'}
      selectOnChange={event => setSelected(event.target.value as string)}
      selectedItem={selected}
      listOfItems={{
        'Sub header one': [{ locale: 'First Item' }, { locale: 'Second Item' }],
        'Sub header two': [{ locale: 'Third Item' }, { locale: 'Fourth Item' }]
      }}
    />
  );
};

export const CustomSelect = () => {
  const [selected, setSelected] = useState('');

  return (
    <SelectInput
      label={inputLabel}
      specialCaseText={'Special case'}
      selectOnChange={event => setSelected(event.target.value as string)}
      selectedItem={selected}
      listOfItems={{
        'Subheader of items': [
          { locale: 'First item' },
          { locale: 'Second item' }
        ]
      }}
    />
  );
};

export const CustomSelectWithSpecialCase = () => {
  const [selected, setSelected] = useState('special-case');

  return (
    <SelectInput
      label={inputLabel}
      specialCaseText={'It is special case'}
      selectOnChange={event => setSelected(event.target.value as string)}
      selectedItem={selected}
      listOfItems={{
        '_First Subheader': [
          { locale: 'First item' },
          { locale: 'Second item' }
        ],
        '_Second Subheader': [
          { locale: 'Third item' },
          { locale: 'Fourth Item' }
        ]
      }}
    />
  );
};

export const CustomSelectWithoutSubHeader = () => {
  const [selected, setSelected] = useState('');

  return (
    <SelectInput
      label={inputLabel}
      specialCaseText={'Special case'}
      selectOnChange={event => setSelected(event.target.value as string)}
      selectedItem={selected}
      listOfItems={{
        '_First Subheader': [
          { locale: 'First item' },
          { locale: 'Second item' }
        ],
        '_Second Subheader': [
          { locale: 'Third item' },
          { locale: 'Fourth Item' }
        ]
      }}
    />
  );
};

export const LongListOfItems = () => {
  const [selected, setSelected] = useState('');

  return (
    <SelectInput
      label={inputLabel}
      specialCaseText={'Special case'}
      selectOnChange={event => setSelected(event.target.value as string)}
      selectedItem={selected}
      listOfItems={{
        '_Subheader of items': [
          ...Array(30)
            .fill({ locale: 'Some stuff' })
            .map((item, index) => ({ locale: item.locale + ' ' + index }))
        ]
      }}
    />
  );
};
