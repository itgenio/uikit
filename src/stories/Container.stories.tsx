import React from 'react';
import { themeDecorator } from './themeDecorator';
import { ContainerStyled } from '../components';

export default {
  title: 'ContainerStyled',
  decorators: [themeDecorator]
};

export const SimpleStyledContainer = () => (
  <ContainerStyled maxWidth="sm">
    <div>
      <p>
        Consequat est consectetur nisi eiusmod deserunt. Incididunt mollit
        officia magna aute irure dolore cillum qui ad sunt voluptate. Proident
        duis incididunt nostrud et proident occaecat dolor ad officia non fugiat
        cupidatat qui. Esse laboris ex est do sunt eu veniam fugiat Lorem
        occaecat sint.
      </p>
    </div>
  </ContainerStyled>
);
