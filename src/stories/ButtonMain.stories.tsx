import React, { Fragment } from 'react';
import { action } from '@storybook/addon-actions';
import { themeDecorator } from './themeDecorator';

import { ButtonPrimary } from '../components/button';
import { ButtonLink } from '../components/button';
import { ButtonWarning } from '../components/button';
import { ButtonSecondary } from '../components/button';
import { ButtonReturn } from '../components/button';

export default {
  title: 'Buttons',
  decorators: [themeDecorator]
};

export const Primary = () => (
  <ButtonPrimary onClick={action('clicked')}>Button Primary</ButtonPrimary>
);

export const Link = () => (
  <ButtonLink onClick={action('clicked')}>Button Link</ButtonLink>
);

export const Warning = () => (
  <ButtonWarning onClick={action('clicked')}>Button Warning</ButtonWarning>
);

export const Secondry = () => (
  <ButtonSecondary>Button Secondry</ButtonSecondary>
);

export const Return = () => (
  <Fragment>
    <ButtonReturn />
    <ButtonReturn onClick={action('click')} />
  </Fragment>
);
