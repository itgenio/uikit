import React from 'react';
import { themeDecorator } from './themeDecorator';
import { FabComponent } from '../components';
import { action } from '@storybook/addon-actions';

export default {
  title: 'Fab component',
  decorators: [themeDecorator]
};

export const FabComponentView = () => (
  <FabComponent onClick={action('click')} visible />
);
