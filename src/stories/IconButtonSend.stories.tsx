import React from 'react';
import { themeDecorator } from './themeDecorator';
import { IconButtonSend } from '../components';
import { action } from '@storybook/addon-actions';

export default {
  title: 'IconButtonSend',
  decarotors: [themeDecorator]
};

export const DefaultIconButton = () => (
  <IconButtonSend onClick={action('click')} />
);

export const LargeIconButton = () => (
  <IconButtonSend onClick={action('click')} fontSize={'large'} />
);
