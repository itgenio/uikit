import React from 'react';
import { themeDecorator } from './themeDecorator';
import { VolumeIndicator } from '../icons';

export default {
  title: 'VolumeIndicator',
  decorators: [themeDecorator]
};

export const ZeroValue = () => <VolumeIndicator gradientEnd={0} />;

export const TwentyValue = () => <VolumeIndicator gradientEnd={20} />;

export const HundredValue = () => <VolumeIndicator gradientEnd={100} />;

export const HalfThousandValue = () => <VolumeIndicator gradientEnd={500} />;

export const ThousandValue = () => <VolumeIndicator gradientEnd={1000} />;
