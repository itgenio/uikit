import React from 'react';
import { themeDecorator } from './themeDecorator';
import { BrandLink } from '../components';

export default {
  title: 'Link',
  decorators: [themeDecorator]
};

export const SimpleBrandLink = () => <BrandLink>Simple Brand Link</BrandLink>;
