import { CSSProperties } from 'react';
import { MuiPickersOverrides } from '@material-ui/pickers/typings/overrides';

type ColorProp = CSSProperties['color'];
type BackgroundColorProp = CSSProperties['backgroundColor'];
type BorderProp = CSSProperties['border'];
type BoxShadowProp = CSSProperties['boxShadow'];

type CommonButtonStyles = {
  backgroundColorHover: BackgroundColorProp;
  backgroundColorPressed: BackgroundColorProp;
  backgroundColorDisabled: BackgroundColorProp;
};

type ContainedButtonStyles = CommonButtonStyles & {
  color: ColorProp;
  colorDisabled: ColorProp;
  backgroundColor: BackgroundColorProp;
  boxShadow: BoxShadowProp;
  boxShadowHover: BoxShadowProp;
};

type OutlinedButtonStyles = CommonButtonStyles & {
  border: BorderProp;
  borderDisabled: BorderProp;
};

declare module '@material-ui/core/styles/createMuiTheme' {
  interface Theme {
    buttons: {
      [buttonType in
        | 'primary'
        | 'secondary'
        | 'warning']: ContainedButtonStyles;
    } & {
      link: OutlinedButtonStyles;
    };
    tabs: { primary: { selectedColor: ColorProp } };
  }

  interface ThemeOptions {
    buttons?: {
      [buttonType in
        | 'primary'
        | 'secondary'
        | 'warning']: ContainedButtonStyles;
    } & {
      link: OutlinedButtonStyles;
    };
    tabs?: { primary: { selectedColor: ColorProp } };
  }
}

type overridesNameToClassKey = {
  [P in keyof MuiPickersOverrides]: keyof MuiPickersOverrides[P];
};

declare module '@material-ui/core/styles/overrides' {
  export interface ComponentNameToClassKey extends overridesNameToClassKey {}
}
