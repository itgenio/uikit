import React from 'react';
import Link, { LinkTypeMap } from '@material-ui/core/Link';
import { makeStyles } from '@material-ui/core';
import { darken } from '@material-ui/core/styles/colorManipulator';
import { OverridableComponent } from '@material-ui/core/OverridableComponent';

const brandColor = '#337ab7';

const useStyles = makeStyles({
  root: {
    color: brandColor,
    cursor: 'pointer',
    textDecoration: 'none',
    '&:hover': {
      color: darken(brandColor, 0.1),
      textDecoration: 'none'
    }
  }
});

export const BrandLink: OverridableComponent<LinkTypeMap> = (props: any) => (
  // TODO: prop underline does not work on portal with hover
  <Link underline="none" classes={useStyles()} {...props} />
);
