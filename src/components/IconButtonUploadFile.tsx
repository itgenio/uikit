import React from 'react';
import IconButton from '@material-ui/core/IconButton';
import AttachFileIcon from '@material-ui/icons/AttachFile';
import DeleteIcon from '@material-ui/icons/Delete';
import { Loader } from './Loader';

export const IconButtonUploadFile = ({
  isReady,
  isLoading,
  buttonId,
  inputId,
  onClick,
  onChange
}: {
  isReady: boolean;
  isLoading: boolean;
  buttonId: string;
  inputId: string;
  onClick: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
  onChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
}) => (
  <IconButton tabIndex={0} id={buttonId} onClick={onClick}>
    {isReady ? (
      <DeleteIcon fontSize="large" />
    ) : isLoading ? (
      <Loader />
    ) : (
      <AttachFileIcon fontSize="large" />
    )}

    <input
      tabIndex={-1}
      type="file"
      id={inputId}
      style={{ display: 'none' }}
      onChange={onChange}
    />
  </IconButton>
);
