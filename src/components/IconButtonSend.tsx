import React, { MutableRefObject } from 'react';

import SendIcon from '@material-ui/icons/Send';
import IconButton, { IconButtonProps } from '@material-ui/core/IconButton';
import { SvgIconProps } from '@material-ui/core';

export const IconButtonSend = ({
  onClick,
  disabled,
  forwardRef,
  fontSize
}: Pick<IconButtonProps, 'onClick' | 'disabled'> &
  Pick<SvgIconProps, 'fontSize'> & {
    forwardRef?: MutableRefObject<HTMLButtonElement | null>;
  }) => (
  <IconButton onClick={onClick} disabled={disabled} ref={forwardRef}>
    <SendIcon fontSize={fontSize} />
  </IconButton>
);
