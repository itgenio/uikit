import React from 'react';
import Container from '@material-ui/core/Container';
import { makeStyles, ContainerProps } from '@material-ui/core';

const styles = makeStyles(() => ({
  root: {
    margin: 'auto',
    position: 'absolute',
    top: '10%'
  }
}));

export const ContainerStyled = (props: ContainerProps) => (
  <Container classes={styles()} {...props} />
);
