import { Theme, withStyles } from '@material-ui/core/styles';
import { Tab, Tabs } from '@material-ui/core';

export const TabsPrimary = withStyles((theme: Theme) => ({
  root: {
    minHeight: 0
  },
  indicator: {
    height: 1,
    backgroundColor: theme.tabs.primary.selectedColor
  }
}))(Tabs);

export const TabPrimary = withStyles((theme: Theme) => ({
  root: {
    minWidth: 0,
    minHeight: 0,
    padding: 0,
    paddingBottom: 6,
    marginRight: 21,
    fontSize: 15,
    fontWeight: 300,
    textTransform: 'none',
    opacity: 1,
    '&:hover': {
      color: theme.tabs.primary.selectedColor,
      fontWeight: 500
    }
  },
  selected: {
    color: theme.tabs.primary.selectedColor,
    fontWeight: 500
  },
  wrapper: {
    lineHeight: '18px'
  }
}))(Tab);
