import React from 'react';
import Divider, {
  DividerTypeMap,
  DividerProps
} from '@material-ui/core/Divider';
import { makeStyles } from '@material-ui/core/styles';
import { OverridableComponent } from '@material-ui/core/OverridableComponent';

const styles = makeStyles(() => ({
  root: {
    backgroundColor: 'unset',
    borderTop: '2px solid',
    opacity: '0.1'
  }
}));

export const DividerStyled: OverridableComponent<DividerTypeMap> = (
  props: DividerProps
) => <Divider classes={styles()} {...props} />;
