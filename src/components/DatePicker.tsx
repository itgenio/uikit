import React from 'react';
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker
} from '@material-ui/pickers';
import MomentUtils from '@date-io/moment';
import { CalendarOutlinedIcon } from '../icons';
import { KeyboardDatePickerProps } from '@material-ui/pickers/DatePicker/DatePicker';

export const DatePicker = (props: KeyboardDatePickerProps) => {
  return (
    <MuiPickersUtilsProvider utils={MomentUtils}>
      <KeyboardDatePicker
        autoOk
        disableToolbar
        disableFuture
        inputVariant="standard"
        variant="inline"
        keyboardIcon={<CalendarOutlinedIcon />}
        format="DD.MM.YYYY"
        {...props}
      />
    </MuiPickersUtilsProvider>
  );
};
