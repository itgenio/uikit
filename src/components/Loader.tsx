import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';

export const Loader = ({
  text = '',
  size = '1.5rem',
  color = 'primary'
}: {
  text?: string;
  size?: string;
  color?: 'primary' | 'secondary';
}) => (
  <span style={{ display: 'inline-flex', alignItems: 'center' }}>
    <CircularProgress size={size} color={color} />
    {!text ? null : <span style={{ marginLeft: '10px' }}>{text}</span>}
  </span>
);
