import React from 'react';
import { Slider } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  root: {
    color: theme.palette.primary.main,
    height: 4
  },
  rail: {
    color: theme.palette.primary.light,
    opacity: 1,
    height: 4
  },
  track: {
    height: 4
  },
  mark: {
    height: 4,
    borderRadius: 0,
    color: 'white'
  },
  markActive: {
    opacity: 1
  },
  thumb: {
    marginTop: -8,
    height: 20,
    width: 20,

    '&::after': {
      content: 'none'
    }
  }
}));

export const RangeSlider = ({
  value,
  min = 0,
  max = 100,
  step,
  steps,
  onChange = () => {},
  onChangeCommitted = () => {}
}: {
  value: number | number[];
  min?: number;
  max?: number;
  step?: number;
  steps: { value: number; label?: string }[];
  onChange?: (event: React.ChangeEvent<{}>, value: number | number[]) => void;
  onChangeCommitted?: (
    event: React.ChangeEvent<{}>,
    value: number | number[]
  ) => void;
}) => {
  const classes = useStyles();

  return (
    <Slider
      classes={classes}
      value={value}
      onChange={onChange}
      onChangeCommitted={onChangeCommitted}
      min={min}
      max={max}
      step={step}
      marks={steps}
    />
  );
};
