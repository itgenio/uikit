import React from 'react';

import TextField, { TextFieldProps } from '@material-ui/core/TextField';

export const TextFieldComponent = (props: TextFieldProps) => (
  <TextField {...props} />
);
