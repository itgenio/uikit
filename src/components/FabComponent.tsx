import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Fab from '@material-ui/core/Fab';
import ArrowUpward from '@material-ui/icons/ArrowUpward';

const FabStyled = withStyles({
  root: {
    position: 'fixed',
    right: 56,
    bottom: 75,
    width: 64,
    height: 64
  }
})(Fab);

export const FabComponent = ({
  onClick,
  visible
}: {
  onClick: () => void;
  visible: boolean;
}) => {
  if (!visible) return null;

  return (
    <FabStyled
      color="primary"
      disableRipple
      children={<ArrowUpward />}
      onClick={onClick}
    />
  );
};
