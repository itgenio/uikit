import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Tooltip, { TooltipProps } from '@material-ui/core/Tooltip';
import HelpOutlineOutlined from '@material-ui/icons/HelpOutlineOutlined';

type Props = Pick<TooltipProps, 'title' | 'placement'> & { maxWidth?: number };

const useStyles = makeStyles(theme => ({
  tooltip: {
    backgroundColor: '#fff',
    color: '#3b3b3b',
    fontSize: 12,
    fontWeight: 500,
    margin: theme.spacing(1),
    border: '1px solid #cfd2d3',
    boxShadow: '0px 2px 20px rgba(0,0,0,0.2)',
    maxWidth: (props: { maxWidth?: number }) => props.maxWidth || 230
  }
}));

export const TooltipHelp: React.FunctionComponent<Props> = ({
  title,
  maxWidth,
  placement
}) => (
  <Tooltip
    disableFocusListener
    disableTouchListener
    placement={placement || 'right'}
    title={title}
    classes={useStyles({ maxWidth })}
  >
    <HelpOutlineOutlined
      color="primary"
      style={{ width: '20px', height: '20px' }}
    />
  </Tooltip>
);
