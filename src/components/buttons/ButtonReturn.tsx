import React from 'react';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardBackspace';
import IconButton from '@material-ui/core/IconButton';

export const ButtonReturn = ({ onClick }: { onClick?: () => void }) => (
  <IconButton
    color="inherit"
    onClick={onClick || (() => window.history.back())}
    children={<KeyboardArrowLeft />}
  />
);
