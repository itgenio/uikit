import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

export const ButtonSecondary = withStyles(theme => ({
  root: {
    backgroundColor: theme.buttons.secondary.backgroundColor,
    boxShadow: theme.buttons.secondary.boxShadow,
    '&:hover': {
      backgroundColor: theme.buttons.secondary.backgroundColorHover,
      boxShadow: theme.buttons.secondary.boxShadowHover
    },
    '&:active': {
      backgroundColor: theme.buttons.secondary.backgroundColorPressed,
      boxShadow: 'none'
    }
  },
  label: {
    color: theme.buttons.secondary.color
  },
  disabled: {
    color: theme.buttons.secondary.colorDisabled,
    backgroundColor: theme.buttons.secondary.backgroundColorDisabled
  }
}))(Button);
