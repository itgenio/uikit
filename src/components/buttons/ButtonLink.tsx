import React from 'react';
import { Theme, makeStyles } from '@material-ui/core/styles';
import Button, { ButtonProps } from '@material-ui/core/Button';

const buttonLinkStyles = makeStyles((theme: Theme) => ({
  root: {
    border: theme.buttons.link.border,
    '&:hover': {
      backgroundColor: theme.buttons.link.backgroundColorHover
    },
    '&:active': {
      backgroundColor: theme.buttons.link.backgroundColorPressed
    }
  },
  label: {
    fontWeight: 'normal'
  },
  disabled: {
    backgroundColor: theme.buttons.link.backgroundColorDisabled,
    border: theme.buttons.link.borderDisabled
  }
}));

export const ButtonLink = (props: ButtonProps) => {
  const classes = buttonLinkStyles(props);

  return (
    <Button
      variant="outlined"
      classes={{
        root: classes.root,
        label: classes.label,
        disabled: classes.disabled
      }}
      {...props}
    >
      {props.children}
    </Button>
  );
};
