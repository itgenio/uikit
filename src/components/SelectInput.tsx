import React from 'react';

import {
  InputLabel,
  MenuItem,
  FormControl,
  Select,
  ListSubheader,
  makeStyles
} from '@material-ui/core';

const selectStyles = makeStyles(() => ({
  formControl: {
    minWidth: 250
  },
  label: {
    backgroundColor: 'white',
    paddingRight: '7px'
  },
  group: {
    height: '3rem',
    lineHeight: '3rem',
    fontSize: 14,
    color: '#aaa'
  },
  hr: {
    margin: 0,
    marginTop: '.5rem',
    borderColor: '#ccc',
    borderWidth: '.5px'
  }
}));

type Items = Record<string, Array<{ locale: string }>>;

export const SelectInput = ({
  label,
  specialCaseText,
  selectedItem,
  selectOnChange,
  listOfItems
}: {
  label: string;
  specialCaseText: string;
  selectedItem?: string;
  listOfItems: Items;
  selectOnChange: (
    event: React.ChangeEvent<{
      name?: string;
      value: unknown;
    }>,
    child: React.ReactNode
  ) => void;
}) => {
  const list = Object.keys(listOfItems);
  const style = selectStyles();

  return (
    <FormControl variant="standard" className={style.formControl}>
      <InputLabel id="select-outlined-label" className={style.label}>
        {label}
      </InputLabel>

      <Select
        labelId="select-outlined-label"
        value={selectedItem || ''}
        onChange={selectOnChange}
      >
        {list.map((value, index) => [
          value[0] === '_' ? null : (
            <ListSubheader className={style.group}>{value}</ListSubheader>
          ),
          listOfItems[value].map(({ locale }) => (
            <MenuItem value={locale} key={locale}>
              {locale}
            </MenuItem>
          )),
          index === list.length - 1 ? null : <hr className={style.hr} />
        ])}

        {selectedItem === 'special-case' ? (
          <MenuItem value={'special-case'} key={'special-case'}>
            {specialCaseText}
          </MenuItem>
        ) : null}
      </Select>
    </FormControl>
  );
};
