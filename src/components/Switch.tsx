import React from 'react';
import MUISwitch, { SwitchProps } from '@material-ui/core/Switch';
import { withStyles, Theme } from '@material-ui/core/styles';

const SwitchStyled = withStyles((theme: Theme) => ({
  switchBase: {
    '&$checked': {
      color: theme.palette.primary.main
    },
    '&$checked:hover': {
      backgroundColor: 'rgba(0, 0, 0, 0.08)'
    },
    '&$checked + $track': {
      backgroundColor: theme.palette.primary.main
    }
  },
  checked: {},
  track: {}
}))(MUISwitch);

export const Switch = ({
  checked,
  disabled,
  onChange
}: Pick<SwitchProps, 'checked' | 'disabled' | 'onChange'>) => {
  return (
    <SwitchStyled
      color="primary"
      checked={checked}
      disabled={disabled}
      onChange={onChange}
    />
  );
};
