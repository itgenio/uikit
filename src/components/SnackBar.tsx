import React, { useState } from 'react';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Snackbar from '@material-ui/core/Snackbar';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';

const AlertBoxStyles = makeStyles(() => ({
  root: {
    position: 'static',
    transform: 'none',
    display: 'inline-block',
    padding: '10px'
  }
}));

const AlertWarning = withStyles(theme => ({
  root: {
    backgroundColor: theme.palette.warning.main
  },
  message: {
    color: theme.palette.warning.contrastText
  }
}))(SnackbarContent);

const AlertInfo = withStyles(theme => ({
  root: {
    backgroundColor: theme.palette.info.main
  },
  message: {
    color: theme.palette.info.contrastText
  }
}))(SnackbarContent);

const AlertAttention = withStyles(theme => ({
  root: {
    backgroundColor: theme.palette.secondary.main
  },
  message: {
    color: theme.palette.secondary.contrastText
  }
}))(SnackbarContent);

const AlertSuccess = withStyles(theme => ({
  root: {
    backgroundColor: theme.palette.success.main
  },
  message: {
    color: theme.palette.success.contrastText
  }
}))(SnackbarContent);

const alertComponentTypes: any = {
  warning: AlertWarning,
  info: AlertInfo,
  attention: AlertAttention,
  success: AlertSuccess
};

export const AlertBox = ({
  type,
  messages
}: {
  type: 'warning' | 'info' | 'attention' | 'success';
  messages: {
    text: string;
    className?: string;
    link?: string;
  }[];
}) => {
  const [open, setOpen] = useState(true);
  const classes = AlertBoxStyles();

  const AlertContent = alertComponentTypes[type];

  const messageElements = messages.map((message, i) => {
    const { className, text, link } = message;

    if (link) {
      return (
        <a key={i} href={link} className={className}>
          {text}{' '}
        </a>
      );
    }

    return (
      <span key={i} className={className}>
        {text}{' '}
      </span>
    );
  });

  return (
    <Snackbar open={open} className={classes.root}>
      <AlertContent
        message={<span className="messages">{messageElements}</span>}
        action={
          <IconButton
            key="close"
            aria-label="close"
            onClick={() => setOpen(false)}
            children={<CloseIcon />}
          />
        }
      />
    </Snackbar>
  );
};
