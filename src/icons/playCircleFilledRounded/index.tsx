import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import SvgIcon, { SvgIconProps } from '@material-ui/core/SvgIcon';

const Icon = withStyles(() => ({
  root: {
    width: 20,
    height: 20
  }
}))(SvgIcon);

export const PlayCircleFilledRoundedIcon = (props: SvgIconProps) => (
  <Icon className="icon" viewBox="0 0 20 20" fill="none" {...props}>
    <path
      d="M10 0C4.47674 0 0 4.47674 0 10C0 15.5232 4.47674 20 10 20C15.5232 20 20 15.5232 20 10C20 4.47674 15.5232 0 10 0ZM14.785 10.8029L8.82012 14.9064C8.65417 15.0201 8.46103 15.0782 8.26737 15.0782C8.11149 15.0782 7.95674 15.0411 7.81423 14.9667C7.49394 14.7973 7.2928 14.4651 7.2928 14.1037V5.89646C7.2928 5.53497 7.49394 5.2028 7.81423 5.03337C8.13451 4.86537 8.52149 4.88851 8.82012 5.09366L14.785 9.19714C15.049 9.37886 15.2071 9.67949 15.2071 10C15.2071 10.3205 15.049 10.6211 14.785 10.8029Z"
      fill="currentColor"
    />
  </Icon>
);
