import React from 'react';
import { SvgIconProps } from '@material-ui/core/SvgIcon';
import AssignmentTurnedInOutlinedIcon from '@material-ui/icons/AssignmentTurnedInOutlined';

// TODO С оригинальной иконкой какой-то косяк, обсудить с Наташей
export const FillingCheckboxOutlinedIcon = (props: SvgIconProps) => (
  <AssignmentTurnedInOutlinedIcon className="icon" {...props} />
);
