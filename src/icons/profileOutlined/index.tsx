import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import SvgIcon, { SvgIconProps } from '@material-ui/core/SvgIcon';

const Icon = withStyles(() => ({
  root: {
    fill: 'none',
    width: 23,
    height: 30
  }
}))(SvgIcon);

export const ProfileOutlinedIcon = (props: SvgIconProps) => (
  <Icon className="icon" viewBox="0 0 23 30" {...props}>
    <path
      d="M11.2679 14.2048C14.2698 14.2048 16.7036 11.2488 16.7036 7.60242C16.7036 3.95596 15.9046 1 11.2679 1C6.63123 1 5.83203 3.95596 5.83203 7.60242C5.83203 11.2488 8.26581 14.2048 11.2679 14.2048Z"
      stroke="currentColor"
      strokeWidth="1.4"
    />
    <path
      d="M1.00107 24.2857C1.00014 24.0633 0.999217 24.223 1.00107 24.2857V24.2857Z"
      stroke="currentColor"
      strokeWidth="1.4"
    />
    <path
      d="M21.5352 24.4593C21.5381 24.3984 21.5362 24.037 21.5352 24.4593V24.4593Z"
      stroke="currentColor"
      strokeWidth="1.4"
    />
    <path
      d="M21.5221 25.0193C21.4214 18.6672 20.5918 16.8572 14.2435 15.7114C14.2435 15.7114 13.3499 16.8501 11.267 16.8501C9.18417 16.8501 8.29039 15.7114 8.29039 15.7114C2.01135 16.8447 1.13131 18.6278 1.01559 24.8129C1.01005 25.1133 1.00001 25.2859 1.00001 25.2859C1.0004 25.3957 1.00086 25.5989 1.00086 25.9532C1.00086 25.9532 2.51224 29.0001 11.267 29.0001C20.0217 29.0001 21.5332 25.9532 21.5332 25.9532C21.5332 25.7256 21.5334 25.5673 21.5336 25.4596L21.5221 25.0193Z"
      stroke="currentColor"
      strokeWidth="1.4"
    />
  </Icon>
);
