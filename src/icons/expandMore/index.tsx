import React from 'react';
import SvgIcon, { SvgIconProps } from '@material-ui/core/SvgIcon';
import { withStyles } from '@material-ui/core/styles';

const Icon = withStyles(() => ({
  root: {
    width: 15,
    height: 11
  }
}))(SvgIcon);

export const ExpandMoreIcon = (props: SvgIconProps) => (
  <Icon className="icon" viewBox="0 0 15 11" fill="none" {...props}>
    <rect
      x="1"
      y="2.38867"
      width="0.795442"
      height="8.9994"
      rx="0.397721"
      transform="rotate(-45 1 2.38867)"
      fill="currentColor"
    />
    <rect
      width="0.795444"
      height="8.99942"
      rx="0.397722"
      transform="matrix(-0.707156 -0.707058 0.707156 -0.707058 7.36401 8.75195)"
      fill="currentColor"
    />
  </Icon>
);
