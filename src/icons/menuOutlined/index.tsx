import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import SvgIcon, { SvgIconProps } from '@material-ui/core/SvgIcon';

const Icon = withStyles(() => ({
  root: {
    width: 26,
    height: 19
  }
}))(SvgIcon);

export const MenuOutlinedIcon = (props: SvgIconProps) => (
  <Icon
    className="icon menu-outlined-icon"
    viewBox="0 0 26 19"
    fill="none"
    {...props}
  >
    <path
      d="M1.5 1H24.5"
      stroke="currentColor"
      strokeWidth="1.4"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M1.5 9H24.5"
      stroke="currentColor"
      strokeWidth="1.4"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M1.5 18H24.5"
      stroke="currentColor"
      strokeWidth="1.4"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </Icon>
);
