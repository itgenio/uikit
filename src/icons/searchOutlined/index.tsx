import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import SvgIcon, { SvgIconProps } from '@material-ui/core/SvgIcon';

const Icon = withStyles(() => ({
  root: {
    width: 26,
    height: 27
  }
}))(SvgIcon);

export const SearchOutlinedIcon = (props: SvgIconProps) => (
  <Icon className="icon" viewBox="0 0 26 27" fill="none" {...props}>
    <path
      d="M25.7751 25.6737L19.4553 19.3539C21.1506 17.4049 22.177 14.862 22.177 12.0826C22.177 5.96464 17.2007 0.994141 11.0885 0.994141C4.9705 0.994141 0 5.97041 0 12.0826C0 18.1949 4.97627 23.1711 11.0885 23.1711C13.8678 23.1711 16.4107 22.1447 18.3597 20.4495L24.6795 26.7693C24.8295 26.9192 25.0313 26.9999 25.2273 26.9999C25.4234 26.9999 25.6252 26.9249 25.7751 26.7693C26.075 26.4694 26.075 25.9735 25.7751 25.6737ZM1.55112 12.0826C1.55112 6.82382 5.82967 2.55103 11.0827 2.55103C16.3415 2.55103 20.6143 6.82958 20.6143 12.0826C20.6143 17.3357 16.3415 21.62 11.0827 21.62C5.82967 21.62 1.55112 17.3414 1.55112 12.0826Z"
      fill="currentColor"
    />
  </Icon>
);
