import { createMuiTheme } from '@material-ui/core/styles';
export const DefaultTheme = createMuiTheme({
    palette: {
        primary: {
            main: '#50965c',
        },
    },
    props: {
        MuiButton: {
            disableTouchRipple: true,
            variant: 'contained',
        },
        MuiIconButton: {
            disableTouchRipple: true,
        },
    },
    overrides: {
        MuiButton: {
            root: {
                height: '36px',
                borderRadius: '6px',
                boxSizing: 'border-box',
            },
            text: {
                padding: '10px 16px',
            },
            label: {
                fontWeight: 500,
                fontSize: '14px',
                lineHeight: '16px',
                textTransform: 'none',
            },
        },
        MuiIconButton: {
            root: {
                margin: 0,
                padding: 0,
            },
        },
        MuiListItem: {
            root: {
                fontSize: '14px',
                fontWeight: 'normal',
            },
        },
        MuiMenuItem: {
            root: {
                fontSize: '14px',
                fontWeight: 'normal',
            },
        },
    },
    buttons: {
        primary: {
            color: 'white',
            colorDisabled: '#6a6a71',
            backgroundColor: '#50965c',
            backgroundColorHover: '#3a7044',
            backgroundColorPressed: '#44634a',
            backgroundColorDisabled: 'rgba(0, 0, 0, 0.1)',
            boxShadow: '0px 2px 20px rgba(80, 150, 92, 0.1)',
            boxShadowHover: ' 0px 2px 20px rgba(58, 112, 68, 0.2)',
        },
        secondary: {
            color: '#50965c',
            colorDisabled: '#6a6a71',
            backgroundColor: '#e0f5e1',
            backgroundColorHover: '#bfecc0',
            backgroundColorPressed: '#aee6b0',
            backgroundColorDisabled: '#ededed',
            boxShadow: '0px 2px 20px rgba(80, 150, 92, 0.1)',
            boxShadowHover: '0px 2px 20px rgba(58, 112, 68, 0.2)',
        },
        warning: {
            color: 'white',
            colorDisabled: '#6a6a71',
            backgroundColor: '#f4c267',
            backgroundColorHover: '#efa724',
            backgroundColorPressed: '#bb7d0d',
            backgroundColorDisabled: 'rgba(0, 0, 0, 0.1)',
            boxShadow: '0px 2px 20px rgba(80, 150, 92, 0.1)',
            boxShadowHover: ' 0px 2px 20px rgba(58, 112, 68, 0.2)',
        },
        link: {
            border: '1px solid #50965c',
            borderDisabled: '1px solid #6a6a71',
            backgroundColorHover: '#e2fae6',
            backgroundColorPressed: '#c9f3d0',
            backgroundColorDisabled: '#ededed',
        },
    },
    tabs: {
        primary: {
            selectedColor: '#3a7044',
        },
    },
    typography: {
        fontSize: 20,
    },
});
//# sourceMappingURL=default.js.map