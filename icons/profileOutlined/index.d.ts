/// <reference types="react" />
import { SvgIconProps } from '@material-ui/core/SvgIcon';
export declare const ProfileOutlinedIcon: (props: SvgIconProps) => JSX.Element;
