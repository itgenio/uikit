/// <reference types="react" />
import { SvgIconProps } from '@material-ui/core/SvgIcon';
export declare const CloseRoundedIcon: (props: SvgIconProps) => JSX.Element;
