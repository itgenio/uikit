import React from 'react';
import SvgIcon from '@material-ui/core/SvgIcon';
import { withStyles } from '@material-ui/core/styles';
const Icon = withStyles(() => ({
    root: {
        width: 14,
        height: 14
    }
}))(SvgIcon);
export const CloseRoundedIcon = (props) => (React.createElement(Icon, Object.assign({ className: "icon", viewBox: "0 0 14 14", fill: "none" }, props),
    React.createElement("path", { d: "M13.8344 0.166125C13.6135 -0.0547238 13.2555 -0.0547238 13.0346 0.166125L0.166124 13.0346C-0.0547237 13.2555 -0.0547237 13.6135 0.166124 13.8344C0.276534 13.9449 0.421287 14 0.566013 14C0.710738 14 0.855464 13.9448 0.965901 13.8344L13.8344 0.965849C14.0552 0.745028 14.0552 0.386973 13.8344 0.166125Z", fill: "currentColor" }),
    React.createElement("path", { d: "M13.8339 13.0341L0.96536 0.165636C0.744539 -0.0552121 0.386457 -0.0552121 0.165636 0.165636C-0.0552121 0.386457 -0.0552121 0.744512 0.165636 0.96536L13.0342 13.8339C13.1446 13.9443 13.2893 13.9995 13.4341 13.9995C13.5788 13.9995 13.7235 13.9443 13.8339 13.8339C14.0547 13.613 14.0547 13.255 13.8339 13.0341Z", fill: "currentColor" })));
//# sourceMappingURL=index.js.map