/// <reference types="react" />
import { SvgIconProps } from '@material-ui/core/SvgIcon';
export declare const AssignmentOutlinedIcon: (props: SvgIconProps) => JSX.Element;
