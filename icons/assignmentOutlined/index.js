import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import SvgIcon from '@material-ui/core/SvgIcon';
const Icon = withStyles(() => ({
    root: {
        width: 22,
        height: 27
    }
}))(SvgIcon);
export const AssignmentOutlinedIcon = (props) => (React.createElement(Icon, Object.assign({ className: "icon", viewBox: "0 0 22 27", fill: "none" }, props),
    React.createElement("path", { d: "M6.18054 2.69581H9.22012C9.49942 2.66468 9.79073 1.55078 10.9854 1.55078C12.1802 1.55078 12.4147 2.69581 12.7508 2.69581H15.797M6.18054 2.69581H2.68164C2.12936 2.69581 1.68164 3.14352 1.68164 3.69581V24.7744C1.68164 25.3267 2.12936 25.7744 2.68164 25.7744H19.4183C19.9706 25.7744 20.4183 25.3267 20.4183 24.7744V3.69581C20.4183 3.14352 19.9706 2.69581 19.4183 2.69581H15.797M6.18054 2.69581V5.25799C6.18054 5.81028 6.62826 6.25799 7.18054 6.25799H14.797C15.3493 6.25799 15.797 5.81028 15.797 5.25799V2.69581", fill: "none", stroke: "currentColor", strokeWidth: "1.4" })));
//# sourceMappingURL=index.js.map