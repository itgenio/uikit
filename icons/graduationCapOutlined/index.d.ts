/// <reference types="react" />
import { SvgIconProps } from '@material-ui/core/SvgIcon';
export declare const GraduationCapOutlinedIcon: (props: SvgIconProps) => JSX.Element;
