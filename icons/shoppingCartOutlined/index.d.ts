/// <reference types="react" />
import { SvgIconProps } from '@material-ui/core/SvgIcon';
export declare const ShoppingCarOutlinedIcon: (props: SvgIconProps) => JSX.Element;
