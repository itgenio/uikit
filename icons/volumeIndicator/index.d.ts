import React from 'react';
export declare const VolumeIndicator: React.ForwardRefExoticComponent<{
    gradientEnd: number;
} & React.RefAttributes<SVGSVGElement>>;
