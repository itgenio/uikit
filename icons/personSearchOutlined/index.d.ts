/// <reference types="react" />
import { SvgIconProps } from '@material-ui/core/SvgIcon';
export declare const PersonSearchOutlinedIcon: (props: SvgIconProps) => JSX.Element;
