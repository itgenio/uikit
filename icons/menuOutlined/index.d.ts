/// <reference types="react" />
import { SvgIconProps } from '@material-ui/core/SvgIcon';
export declare const MenuOutlinedIcon: (props: SvgIconProps) => JSX.Element;
