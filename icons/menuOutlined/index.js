import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import SvgIcon from '@material-ui/core/SvgIcon';
const Icon = withStyles(() => ({
    root: {
        width: 26,
        height: 19
    }
}))(SvgIcon);
export const MenuOutlinedIcon = (props) => (React.createElement(Icon, Object.assign({ className: "icon menu-outlined-icon", viewBox: "0 0 26 19", fill: "none" }, props),
    React.createElement("path", { d: "M1.5 1H24.5", stroke: "currentColor", strokeWidth: "1.4", strokeLinecap: "round", strokeLinejoin: "round" }),
    React.createElement("path", { d: "M1.5 9H24.5", stroke: "currentColor", strokeWidth: "1.4", strokeLinecap: "round", strokeLinejoin: "round" }),
    React.createElement("path", { d: "M1.5 18H24.5", stroke: "currentColor", strokeWidth: "1.4", strokeLinecap: "round", strokeLinejoin: "round" })));
//# sourceMappingURL=index.js.map