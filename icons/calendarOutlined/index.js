import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import SvgIcon from '@material-ui/core/SvgIcon';
const iconStyle = makeStyles(() => ({
    root: {
        width: 22,
        height: 27
    }
}));
export const CalendarOutlinedIcon = (props) => (React.createElement(SvgIcon, Object.assign({ classes: { ...iconStyle(), ...props.classes }, className: "icon", viewBox: "0 0 17 18", fill: "none" }, props),
    React.createElement("path", { d: "M1 6.5V14C1 15.6569 2.34315 17 4 17H13C14.6569 17 16 15.6569 16 14V6.5M1 6.5V5C1 3.34315 2.34315 2 4 2H13C14.6569 2 16 3.34315 16 5V6.5M1 6.5H16", stroke: "currentColor", strokeWidth: "0.8", fill: "none" }),
    React.createElement("path", { d: "M6 1V3", stroke: "currentColor", strokeWidth: "0.8", strokeLinecap: "round", strokeLinejoin: "round" }),
    React.createElement("path", { d: "M11 1V3", stroke: "currentColor", strokeWidth: "0.8", strokeLinecap: "round", strokeLinejoin: "round" }),
    React.createElement("circle", { cx: "4", cy: "10", r: "1", fill: "currentColor" }),
    React.createElement("circle", { cx: "7", cy: "10", r: "1", fill: "currentColor" }),
    React.createElement("circle", { cx: "10", cy: "10", r: "1", fill: "currentColor" }),
    React.createElement("circle", { cx: "13", cy: "10", r: "1", fill: "currentColor" }),
    React.createElement("circle", { cx: "4", cy: "13", r: "1", fill: "currentColor" }),
    React.createElement("circle", { cx: "7", cy: "13", r: "1", fill: "currentColor" }),
    React.createElement("circle", { cx: "10", cy: "13", r: "1", fill: "currentColor" }),
    React.createElement("circle", { cx: "13", cy: "13", r: "1", fill: "currentColor" })));
//# sourceMappingURL=index.js.map