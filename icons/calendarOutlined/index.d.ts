/// <reference types="react" />
import { SvgIconProps } from '@material-ui/core/SvgIcon';
export declare const CalendarOutlinedIcon: (props: SvgIconProps) => JSX.Element;
