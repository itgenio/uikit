/// <reference types="react" />
import { SvgIconProps } from '@material-ui/core/SvgIcon';
export declare const FolderOutlinedIcon: (props: SvgIconProps) => JSX.Element;
