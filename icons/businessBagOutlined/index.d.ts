/// <reference types="react" />
import { SvgIconProps } from '@material-ui/core/SvgIcon';
export declare const BusinessBagOutlinedIcon: (props: SvgIconProps) => JSX.Element;
