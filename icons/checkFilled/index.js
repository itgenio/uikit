import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import SvgIcon from '@material-ui/core/SvgIcon';
const Icon = withStyles(() => ({
    root: {
        width: 23,
        height: 28
    }
}))(SvgIcon);
export const CheckFilledIcon = (props) => (React.createElement(Icon, Object.assign({ className: "icon", viewBox: "0 0 23 28" }, props),
    React.createElement("path", { d: "M2.9255 15.2903L9.10946 23.1963L21.0001 4.16803", stroke: "currentColor", strokeWidth: "3", strokeLinecap: "round", fill: "none" })));
//# sourceMappingURL=index.js.map