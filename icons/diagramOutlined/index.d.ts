/// <reference types="react" />
import { SvgIconProps } from '@material-ui/core/SvgIcon';
export declare const DiagramOutlinedIcon: (props: SvgIconProps) => JSX.Element;
