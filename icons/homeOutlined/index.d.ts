/// <reference types="react" />
import { SvgIconProps } from '@material-ui/core/SvgIcon';
export declare const HomeOutlinedIcon: (props: SvgIconProps) => JSX.Element;
