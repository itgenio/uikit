/// <reference types="react" />
import { SvgIconProps } from '@material-ui/core/SvgIcon';
export declare const BookOpenedOutlinedIcon: (props: SvgIconProps) => JSX.Element;
