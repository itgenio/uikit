/// <reference types="react" />
import { SvgIconProps } from '@material-ui/core/SvgIcon';
export declare const ArrowLeftIosIcon: (props: SvgIconProps) => JSX.Element;
