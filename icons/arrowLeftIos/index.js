import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import SvgIcon from '@material-ui/core/SvgIcon';
const Icon = withStyles(() => ({
    root: {
        width: 12,
        height: 12
    }
}))(SvgIcon);
export const ArrowLeftIosIcon = (props) => (React.createElement(Icon, Object.assign({ className: "icon", viewBox: "0 0 12 12", fill: "none" }, props),
    React.createElement("rect", { width: "0.707106", height: "7.99999", rx: "0.353553", transform: "matrix(0.707107 -0.707107 -0.707107 -0.707107 6.07227 12.1445)", fill: "currentColor" }),
    React.createElement("rect", { width: "0.707106", height: "7.99999", rx: "0.353553", transform: "matrix(0.707107 0.707107 0.707107 -0.707107 0.415039 6.4873)", fill: "currentColor" })));
//# sourceMappingURL=index.js.map