import React from 'react';
import AssignmentTurnedInOutlinedIcon from '@material-ui/icons/AssignmentTurnedInOutlined';
// TODO С оригинальной иконкой какой-то косяк, обсудить с Наташей
export const FillingCheckboxOutlinedIcon = (props) => (React.createElement(AssignmentTurnedInOutlinedIcon, Object.assign({ className: "icon" }, props)));
//# sourceMappingURL=index.js.map