/// <reference types="react" />
import { SvgIconProps } from '@material-ui/core/SvgIcon';
export declare const FillingCheckboxOutlinedIcon: (props: SvgIconProps) => JSX.Element;
