import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import SvgIcon from '@material-ui/core/SvgIcon';
const Icon = withStyles(() => ({
    root: {
        width: 12,
        height: 12
    }
}))(SvgIcon);
export const ArrowRightIosIcon = (props) => (React.createElement(Icon, Object.assign({ className: "icon", viewBox: "0 0 12 12", fill: "none" }, props),
    React.createElement("rect", { x: "6.34277", y: "11.834", width: "0.707106", height: "7.99999", rx: "0.353553", transform: "rotate(-135 6.34277 11.834)", fill: "currentColor" }),
    React.createElement("rect", { x: "12", y: "6.17676", width: "0.707106", height: "7.99999", rx: "0.353553", transform: "rotate(135 12 6.17676)", fill: "currentColor" })));
//# sourceMappingURL=index.js.map