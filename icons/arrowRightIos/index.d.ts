/// <reference types="react" />
import { SvgIconProps } from '@material-ui/core/SvgIcon';
export declare const ArrowRightIosIcon: (props: SvgIconProps) => JSX.Element;
