/// <reference types="react" />
import { SvgIconProps } from '@material-ui/core/SvgIcon';
export declare const ExpandLessIcon: (props: SvgIconProps) => JSX.Element;
