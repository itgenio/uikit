import React from 'react';
import SvgIcon from '@material-ui/core/SvgIcon';
import { withStyles } from '@material-ui/core/styles';
const Icon = withStyles(() => ({
    root: {
        width: 15,
        height: 11
    }
}))(SvgIcon);
export const ExpandLessIcon = (props) => (React.createElement(Icon, Object.assign({ className: "icon", viewBox: "0 0 15 11", fill: "none" }, props),
    React.createElement("rect", { width: "0.795442", height: "8.9994", rx: "0.397721", transform: "matrix(0.707107 0.707107 0.707107 -0.707107 1 8)", fill: "currentColor" }),
    React.createElement("rect", { width: "0.795444", height: "8.99942", rx: "0.397722", transform: "matrix(-0.707156 0.707058 0.707156 0.707058 7.36401 1.63672)", fill: "currentColor" })));
//# sourceMappingURL=index.js.map