/// <reference types="react" />
import { SvgIconProps } from '@material-ui/core/SvgIcon';
export declare const ChatOutlinedIcon: (props: SvgIconProps) => JSX.Element;
